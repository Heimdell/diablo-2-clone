module D2.Logic where

import Control.Monad.State
import Control.Lens

data GameState =
  GameState {
    _mapVisible :: Bool,
    _gamePaused :: Bool
  } deriving (Show)

makeLenses ''GameState

initGameState :: GameState
initGameState =
  GameState {
    _mapVisible = False,
    _gamePaused = False
  }

toggleMap :: State GameState ()
toggleMap = mapVisible %= not

togglePause :: State GameState ()
togglePause = gamePaused %= not
