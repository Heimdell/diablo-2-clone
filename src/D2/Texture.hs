module D2.Texture where

import Data.Word

data TextureGenerator =
  TextureGenerator {
    tgWidth, tgHeight :: Int,
    tgFill :: forall m. Monad m => ((Int, Int) -> Word8 -> m ()) -> m ()
  }
