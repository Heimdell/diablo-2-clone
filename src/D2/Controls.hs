module D2.Controls where

import Data.Map as Map

-- Actions that can be triggered by a key press.
data ButtonAction
  = ActionCharacterScreen
  | ActionInventoryScreen
  | ActionPartyScreen
  | ActionMessageLog
  | ActionQuestLog
  | ActionHelpScreen
  | ActionSkillTree
  | ActionSkillSpeedBar
  | ActionSkill1
  | ActionSkill2
  | ActionSkill3
  | ActionSkill4
  | ActionSkill5
  | ActionSkill6
  | ActionSkill7
  | ActionSkill8
  | ActionSelectPreviousSkill
  | ActionSelectNextSkill
  | ActionShowBelt
  | ActionUseBelt1
  | ActionUseBelt2
  | ActionUseBelt3
  | ActionUseBelt4
  | ActionChat
  | ActionRun
  | ActionToggleRunWalk
  | ActionStandStill
  | ActionShowItems
  | ActionShowPortraits
  | ActionAutomap
  | ActionCenterAutomap
  | ActionFadeAutomap
  | ActionPartyOnAutomap
  | ActionNamesOnAutomap
  | ActionSayHelp
  | ActionSayFollowMe
  | ActionSayThisIsForYou
  | ActionSayThanks
  | ActionSaySorry
  | ActionSayBye
  | ActionSayNowYouDie
  | ActionSayRetreat
  | ActionScreenShot
  | ActionClearScreen
  | ActionClearMessages
  deriving (Eq, Ord, Show, Enum, Bounded)

-- Known buttons that can be pressed (including keyboard and mouse buttons).
data ButtonTag
  = KeyboardButton_A
  | KeyboardButton_B
  | KeyboardButton_C
  | KeyboardButton_D
  | KeyboardButton_E
  | KeyboardButton_F
  | KeyboardButton_G
  | KeyboardButton_H
  | KeyboardButton_I
  | KeyboardButton_J
  | KeyboardButton_K
  | KeyboardButton_L
  | KeyboardButton_M
  | KeyboardButton_N
  | KeyboardButton_O
  | KeyboardButton_P
  | KeyboardButton_Q
  | KeyboardButton_R
  | KeyboardButton_S
  | KeyboardButton_T
  | KeyboardButton_U
  | KeyboardButton_V
  | KeyboardButton_W
  | KeyboardButton_X
  | KeyboardButton_Y
  | KeyboardButton_Z
  | KeyboardButton_1
  | KeyboardButton_2
  | KeyboardButton_3
  | KeyboardButton_4
  | KeyboardButton_5
  | KeyboardButton_6
  | KeyboardButton_7
  | KeyboardButton_8
  | KeyboardButton_9
  | KeyboardButton_0
  | KeyboardButton_NumPad1
  | KeyboardButton_NumPad2
  | KeyboardButton_NumPad3
  | KeyboardButton_NumPad4
  | KeyboardButton_NumPad5
  | KeyboardButton_NumPad6
  | KeyboardButton_NumPad7
  | KeyboardButton_NumPad8
  | KeyboardButton_NumPad9
  | KeyboardButton_NumPad0
  | KeyboardButton_F1
  | KeyboardButton_F2
  | KeyboardButton_F3
  | KeyboardButton_F4
  | KeyboardButton_F5
  | KeyboardButton_F6
  | KeyboardButton_F7
  | KeyboardButton_F8
  | KeyboardButton_F9
  | KeyboardButton_F10
  | KeyboardButton_F11
  | KeyboardButton_F12
  | KeyboardButton_Tilde
  | KeyboardButton_Enter
  | KeyboardButton_Ctrl
  | KeyboardButton_Shift
  | KeyboardButton_Alt
  | KeyboardButton_Tab
  | KeyboardButton_Space
  | KeyboardButton_Esc
  | KeyboardButton_PrintScreen
  | MouseButton_LeftClick
  | MouseButton_RightClick
  | MouseButton_MiddleClick
  | MouseButton_WheelUp
  | MouseButton_WheelDown
  deriving (Eq, Ord, Show, Enum, Bounded)

type UserButtonConfig = ButtonAction -> [ButtonTag]

defaultUserButtonConfig :: UserButtonConfig
defaultUserButtonConfig = \case
  ActionCharacterScreen -> [KeyboardButton_A, KeyboardButton_C]
  ActionInventoryScreen -> [KeyboardButton_I, KeyboardButton_B]
  ActionPartyScreen -> [KeyboardButton_P]
  ActionMessageLog -> [KeyboardButton_M]
  ActionQuestLog -> [KeyboardButton_Q]
  ActionHelpScreen -> [KeyboardButton_H]
  ActionSkillTree -> [KeyboardButton_T]
  ActionSkillSpeedBar -> [KeyboardButton_S]
  ActionSkill1 -> [KeyboardButton_F1]
  ActionSkill2 -> [KeyboardButton_F2]
  ActionSkill3 -> [KeyboardButton_F3]
  ActionSkill4 -> [KeyboardButton_F4]
  ActionSkill5 -> [KeyboardButton_F5]
  ActionSkill6 -> [KeyboardButton_F6]
  ActionSkill7 -> [KeyboardButton_F7]
  ActionSkill8 -> [KeyboardButton_F8]
  ActionSelectPreviousSkill -> [MouseButton_WheelUp]
  ActionSelectNextSkill -> [MouseButton_WheelDown]
  ActionShowBelt -> [KeyboardButton_Tilde]
  ActionUseBelt1 -> [KeyboardButton_1]
  ActionUseBelt2 -> [KeyboardButton_2]
  ActionUseBelt3 -> [KeyboardButton_3]
  ActionUseBelt4 -> [KeyboardButton_4]
  ActionChat -> [KeyboardButton_Enter]
  ActionRun -> [KeyboardButton_Ctrl]
  ActionToggleRunWalk -> [KeyboardButton_R]   -- TODO: also Mouse 5
  ActionStandStill -> [KeyboardButton_Shift]
  ActionShowItems -> [KeyboardButton_Alt]     -- TODO: also Mouse 4
  ActionShowPortraits -> [KeyboardButton_Z]
  ActionAutomap -> [KeyboardButton_Tab]       -- TODO: also Mouse 3
  ActionCenterAutomap -> [KeyboardButton_F9]
  ActionFadeAutomap -> [KeyboardButton_F10]
  ActionPartyOnAutomap -> [KeyboardButton_F11]
  ActionNamesOnAutomap -> [KeyboardButton_F12]
  ActionSayHelp -> [KeyboardButton_NumPad0]
  ActionSayFollowMe -> [KeyboardButton_NumPad1]
  ActionSayThisIsForYou -> [KeyboardButton_NumPad2]
  ActionSayThanks -> [KeyboardButton_NumPad3]
  ActionSaySorry -> [KeyboardButton_NumPad4]
  ActionSayBye -> [KeyboardButton_NumPad5]
  ActionSayNowYouDie -> [KeyboardButton_NumPad6]
  ActionSayRetreat -> [KeyboardButton_NumPad7]
  ActionScreenShot -> [KeyboardButton_PrintScreen]
  ActionClearScreen -> [KeyboardButton_Space]
  ActionClearMessages -> [KeyboardButton_N]

newtype ButtonInterpreter = ButtonInterpreter { interpretButton :: ButtonTag -> Maybe ButtonAction }

invertUserButtonConfig :: UserButtonConfig -> ButtonInterpreter
invertUserButtonConfig conf = ButtonInterpreter (\bt -> Map.lookup bt buttonMap)
  where
    -- TODO: probably use an Array for faster lookups
    buttonMap  :: Map ButtonTag ButtonAction
    buttonMap = Map.fromList do
      action :: ButtonAction <- [minBound .. maxBound]
      tag <- conf action
      [(tag, action)]

