module D2.Resources.Parser.Binary where

import Control.Applicative
import Control.Lens
import Control.Monad.State
import Control.Monad.Except

import Data.ByteString.Lazy qualified as Bytes
import Data.ByteString.Lazy (ByteString)
import Data.ByteString.Builder qualified as Builder
import Data.Int
import Data.Word
import Data.Bits
import Data.Foldable
import Data.List.Split

import System.PosixCompat.Files (getFileStatus, fileSize)
import System.PosixCompat.Types (COff (..))

data PState = PState
  { _psInput :: ByteString
  , _psPos   :: Int64
  , _psSize  :: Int64
  }

makeLenses ''PState

newtype Parser a = Parser
  { unParser :: StateT PState (Except PError) a
  }
  deriving newtype (Functor, Applicative, Alternative, MonadError PError, Monad, MonadState PState)

parseFile :: String -> Parser a -> IO (Either PError a)
parseFile name parser = do
  COff size <- fileSize <$> getFileStatus name
  input     <- Bytes.readFile name
  return $ runParser input size parser

runParser :: ByteString -> Int64 -> Parser a -> Either PError a
runParser _psInput _psSize (Parser p) =
  runExcept $ p `evalStateT` PState { _psInput, _psSize, _psPos = 0 }

instance Show PState where
  show PState {_psPos, _psSize} = "PState " ++ show _psPos ++ "/" ++ show _psSize

data PError
  = EOS                             PState
  | Expected          ByteString    PState
  | ExpectedWord32    Word32        PState
  | ExpectedDiapasone Word32 Word32 PState
  | InternalError
  deriving stock Show

instance Monoid PError where
  mempty = InternalError

instance Semigroup PError where
  _ <> b = b

die :: (PState -> PError) -> Parser a
die ctor = do
  gets ctor >>= throwError

byte :: Parser Word8
byte = do
  ps <- get
  if ps^.psPos < ps^.psSize
  then do
    let b = Bytes.index (ps^.psInput) (ps^.psPos)
    psPos += 1
    return b
  else do
    die EOS

withDecryption :: ([Word32] -> [Word32]) -> Int64 -> Int64 -> Parser a -> Parser a
withDecryption decrypt start size parser = do
  old <- get
  psInput %= Bytes.take size . Bytes.drop start
  psInput %= asWordStream decrypt
  psPos .= 0
  res <- parser
  put old
  return res

asWordStream :: ([Word32] -> [Word32]) -> ByteString -> ByteString
asWordStream f bytes = do
  let wordz  = map pack32 $ chunksOf 4 $ Bytes.unpack bytes
  let words' = f wordz
  Builder.toLazyByteString $ foldMap Builder.word32LE words'

times :: Parser a -> Int -> Parser [a]
times p n = sequence (replicate n p)

word32LE :: Parser Word32
word32LE = do
  bs <- byte `times` 4
  return $ pack32 bs

pack32 :: [Word8] -> Word32
pack32 = foldr push 0

word16LE :: Parser Word16
word16LE = do
  bs <- byte `times` 2
  return $ pack16 bs

int16LE :: Parser Int16
int16LE = word16LE <&> fromIntegral

pack16 :: [Word8] -> Word16
pack16 = foldr push 0

pack16int :: [Int8] -> Int16
pack16int = foldr push 0

push :: (Bits a1, Integral a2, Num a1) => a2 -> a1 -> a1
push b w = fromIntegral b .|. (w `shiftL` 8)

word32BE :: Parser Word32
word32BE = byteSwap32 <$> word32LE

word16BE :: Parser Word16
word16BE = byteSwap16 <$> word16LE

ascii :: ByteString -> Parser ()
ascii sig = do
  ps <- get
  let bs = Bytes.unpack sig
  for_ bs \b -> do
    b' <- byte
    unless (b == b') do
      throwError $ Expected sig ps

jumpTo :: Word32 -> Parser a -> Parser a
jumpTo pos p = do
  old <- use psPos
  psPos .= fromIntegral pos
  res <- p
  psPos .= old
  return res

thatIs :: Parser Word32 -> Word32 -> Parser Word32
thatIs p a = do
  a' <- p
  unless (a == a') do
    die $ ExpectedWord32 a
  return a'

thatIsIn :: Parser Word32 -> (Word32, Word32) -> Parser Word32
thatIsIn p (l, h) = do
  a' <- p
  unless (l <= a' && a' <= h) do
    die $ ExpectedDiapasone l h
  return a'

block :: Word32 -> Parser ByteString
block len = do
  pos <- use psPos
  inp <- use psInput
  return $ Bytes.take (fromIntegral len) $ Bytes.drop pos inp
