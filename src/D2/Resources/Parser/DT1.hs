
module D2.Resources.Parser.DT1 where

import Control.Applicative
import Control.Lens
import Control.Monad.State

import Data.Array
import Data.Word
import Data.Int
import Data.Foldable
import Data.Traversable
import Data.ByteString.Lazy qualified as Bytes
import Data.ByteString.Lazy (ByteString)
import Data.Map qualified as Map
import Data.Map (Map)

import D2.Resources.Parser.Binary

data Point = Point { _curX, _curY :: Int }

makeLenses ''Point

data Header = Header
  { tileHeaders :: Array Word32 TileHeader
  }
  deriving stock (Show)

data TileHeader = TileHeader
  { direction     :: Word32
  , roofHeight    :: Word16
  , soundIndex    :: Word8
  , isAnimated    :: Bool
  , height        :: Word32
  , width         :: Word32
  , orientation   :: Word32
  , mainIndex     :: Word32
  , subIndex      :: Word32
  , rarityOrFrame :: Word32
  , subTileFlags  :: [Word8]
  , blockHeaders  :: [BlockHeader]
  }
  deriving stock (Show)

data BlockHeader = BlockHeader
  { posX,  posY  :: Int16
  , gridX, gridY :: Word8
  , format       :: Word16
  , data_        :: Filler
  }
  deriving stock (Show)

data Filler = Filler
  { fWidth    :: Int
  , fHeight   :: Int
  , fFill     :: forall m color. Monad m => (Word8 -> color) -> ((Int, Int) -> color -> m ()) -> m ()
  }

instance Show Filler where
  show _ = "-"

header :: Parser Header
header = do
  word32LE `thatIs` 7
  word32LE `thatIs` 6
  (word32LE `thatIs` 0) `times` (260 `div` 4)
  tileCount  <- word32LE
  tileOffset <- word32LE

  psPos .= fromIntegral tileOffset

  headers <- many tileHeader
  return $ Header $ listArray (0, tileCount - 1) headers

tileHeader :: Parser TileHeader
tileHeader = do
  direction     <- word32LE `thatIsIn` (1, 5)
  roofHeight    <- word16LE
  soundIndex    <- byte
  isAnimated    <- byte     <&> odd
  height        <- word32LE
  width         <- word32LE
  _             <- word32LE `thatIs` 0
  orientation   <- word32LE `thatIsIn` (0, 63)
  mainIndex     <- word32LE `thatIsIn` (0, 63)
  subIndex      <- word32LE `thatIsIn` (0, 63)
  rarityOrFrame <- word32LE
  _             <- word32LE
  subTileFlags  <- byte `times` 25
  _             <- byte `times` 7
  bhs           <- word32LE
  _bdlen        <- word32LE
  blcs          <- word32LE
  _             <- (word32LE `thatIs` 0) `times` 3
  blockHeaders  <- jumpTo bhs $ blockHeader bhs `times` (fromIntegral blcs)
  return TileHeader {..}

blockHeader :: Word32 -> Parser BlockHeader
blockHeader base = do
  posX   <- int16LE
  posY   <- int16LE
  _      <- int16LE
  gridX  <- byte
  gridY  <- byte
  format <- word16LE
  len    <- word32LE
  _      <- word16LE
  offset <- word32LE
  data_  <- jumpTo (base + offset) do
    block len <&> decode format

  return BlockHeader {..}

decode :: Word16 -> ByteString -> Filler
decode fmt input = do
  if fmt == 1 then do
    isometric input
  else do
    raw input

isometric :: ByteString -> Filler
isometric input = Filler
  { fHeight = 15
  , fWidth  = 32
  , fFill   = \palette putpixel -> do
      let xjump = [14, 12, 10, 8, 6, 4, 2, 0, 2, 4, 6, 8, 10, 12, 14]
      let nbpix = [4, 8, 12, 16, 20, 24, 28, 32, 28, 24, 20, 16, 12, 8, 4]
      flip evalStateT input do
        for_ (zip3 [0..] xjump nbpix) \(y, xjump0, nbpix0) -> do
          for_ [xjump0.. xjump0 + nbpix0 - 1] \x -> do
            gets Bytes.uncons >>= \case
              Nothing -> error $ "pixel data too short " ++ show (x, y)
              Just (pcx, rest) -> do
                put rest
                lift $ putpixel (x, y) (palette pcx)
  }

{-
-}

{-
void draw_block_isometric (BITMAP * dst, int x0, int y0, const UBYTE * data, int length)
{
   UBYTE * ptr = data;
   int   x, y=0, n,
         xjump[15] = {14, 12, 10, 8, 6, 4, 2, 0, 2, 4, 6, 8, 10, 12, 14},
         nbpix[15] = {4, 8, 12, 16, 20, 24, 28, 32, 28, 24, 20, 16, 12, 8, 4};


   // 3d-isometric subtile is 256 bytes, no more, no less
   if (length != 256)
      return;


   // draw
   while (length > 0)
   {
      x = xjump[y];
      n = nbpix[y];
      length -= n;
      while (n)
      {
         putpixel(dst, x0+x, y0+y, * ptr);
         ptr++;
         x++;
         n--;
      }
      y++;
   }
}
-}

raw :: ByteString -> Filler
raw input = Filler
  { fHeight = 32
  , fWidth  = 32
  , fFill   = \palette putpixel -> do
      flip evalStateT (Point 0 0) do
        for_ (strokes input) \case
          Skip n -> curX += fromIntegral n
          Draw pcs -> do
            for_ pcs \pcx -> do
              Point x' y' <- get
              lift $ putpixel (x', y') (palette pcx)
              curX += 1
          NextLine -> do
            curY += 1
            curX .= 0
  }

data RLE
  = Skip  Word8
  | Draw [Word8]
  | NextLine

strokes :: ByteString -> [RLE]
strokes bs = case Bytes.uncons bs of
  Just (skip, bs') -> case Bytes.uncons bs' of
    Just (draw, bs'') ->
      case (skip, draw) of
        (0, 0) -> NextLine : strokes bs''
        _ -> do
          let draw' = fromIntegral draw
          let taken = Bytes.unpack $ Bytes.take draw' bs''
          let rest  = Bytes.drop draw' bs''
          Skip skip : Draw taken : strokes rest
    Nothing -> []
  Nothing -> []

testFiller :: Filler -> Map (Int, Int) Word8
testFiller (Filler _ _ run) = flip evalState Map.empty $ do
  run id \(x, y) c -> modify $ Map.insert (x, y) c
  get

{-
void draw_block_normal (BITMAP * dst, int x0, int y0, const UBYTE * data, int length)
{
   UBYTE * ptr = data, b1, b2;
   int   x=0, y=0;


   // draw
   while (length > 0)
   {
      b1 = * ptr;
      b2 = * (ptr + 1);
      ptr += 2;
      length -= 2;
      if (b1 || b2)
      {
         x += b1;
         length -= b2;
         while (b2)
         {
            putpixel(dst, x0+x, y0+y, * ptr);
            ptr++;
            x++;
            b2--;
         }
      }
      else
      {
         x = 0;
         y++;
      }
   }
}
-}

texture :: TileHeader -> Filler
texture tile =
  case format (head (blockHeaders tile)) of
    1 -> isoTexture   tile
    _ -> plainTexture tile

isoTexture :: TileHeader -> Filler
isoTexture tile = Filler
  { fHeight = 16 * 5
  , fWidth  = 32 * 5
  , fFill   = \palette putpixel -> do
      for_ (blockHeaders tile) \block' -> do
        let x   = gridX block' & fromIntegral
        let y   = gridY block' & fromIntegral
        let ex  = x + y
        let wai = x - y + 4
        fFill (data_ block') palette (offsetFill (ex * 16, wai * 8) putpixel)
  }

plainTexture :: TileHeader -> Filler
plainTexture tile = Filler
  { fHeight = 256
  , fWidth  = 256
  , fFill   = \palette putpixel -> do
      for_ (blockHeaders tile) \block' -> do
        let x = posX block' & fromIntegral
        let y = posY block' & fromIntegral & (+ 256)
        fFill (data_ block') palette (offsetFill (x, y) putpixel)
  }
  where
    blocks = blockHeaders tile
    gridW  = maximum (map posX blocks) - minimum (map posX blocks) + 32
    gridH  = maximum (map posY blocks) - minimum (map posY blocks) + 32

offsetFill :: Monad m => (Int, Int) -> ((Int, Int) -> color -> m ()) -> (Int, Int) -> color -> m ()
offsetFill (dx, dy) k (x, y) c = k (x + dx, y + dy) c

data Palette = Palette { getPalette :: forall c. (Word8 -> Word8 -> Word8 -> c) -> Array Word8 c }

palette :: Parser Palette
palette = do
  triplets <- for [0.. 255] \_ -> do
    r <- byte
    g <- byte
    b <- byte
    return (r, g, b)
  return $ Palette \c -> listArray (0, 255) [c r g b | (r, g, b) <- triplets]
