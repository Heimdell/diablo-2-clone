module D2.Driver.Game (appGame) where

import Data.Foldable
import Data.Array as A
import Data.Word (Word8)
import Control.Monad.State
import Control.Lens
import System.IO.Unsafe
import SDL qualified

import qualified D2.Resources.Parser.Binary as R
import qualified D2.Resources.Parser.DT1 as R.DT1
import D2.Texture
import D2.Controls
import D2.Logic
import D2.SDL.Util

data AppState = AppState
  { _appWindow :: SDL.Window,
    _appRenderer :: SDL.Renderer,
    _appButtonInterpreter :: ButtonInterpreter,
    _appGameState :: GameState
  }

makeLenses ''AppState

appGame :: IO ()
appGame = do
  -- TODO: redirect stdout/stderr to avoid silent crashes on Windows
  setExceptionHandler (Nothing @SDL.Window)
  SDL.initialize [SDL.InitVideo]
  window <-
    SDL.createWindow
      "Diablo II Clone"
      SDL.defaultWindow
        { SDL.windowInitialSize = SDL.V2 800 600,
          SDL.windowInputGrabbed = True
        }
  setExceptionHandler (Just window)
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer
  let appState =
        AppState {
          _appWindow = window,
          _appRenderer = renderer,
          _appButtonInterpreter = invertUserButtonConfig defaultUserButtonConfig,
          _appGameState = initGameState
        }
  appRedraw appState   -- Draw the window for the first time,
                       -- before we handle any events.
  appLoop appState
  SDL.quit

appLoop :: AppState -> IO ()
appLoop appState = do
  -- TODO: handle event queue using
  --   liftA2 (:) SDL.waitEvent SDL.pollEvents
  event <- SDL.waitEvent
  case SDL.eventPayload event of
    SDL.QuitEvent -> return ()
    SDL.KeyboardEvent kd
      | Just btnTag <- matchButton (SDL.keyboardEventKeyMotion kd) (SDL.keyboardEventKeysym kd)
      , Just btnAct <- interpretButton (view appButtonInterpreter appState) btnTag
      -> do
        let appState' = execState (zoom appGameState (applyButtonAction btnAct)) appState
        print btnAct
        print (view appGameState appState')
        appRedraw appState'
        appLoop appState'
    _ -> do
      -- Unknown event
      -- print event
      appLoop appState

applyButtonAction :: ButtonAction -> State GameState ()
applyButtonAction ActionAutomap = toggleMap
applyButtonAction _ = return ()

matchButton :: SDL.InputMotion -> SDL.Keysym -> Maybe ButtonTag
matchButton SDL.Pressed ks =
  case SDL.keysymKeycode ks of
    SDL.KeycodeA -> Just KeyboardButton_A
    SDL.KeycodeB -> Just KeyboardButton_B
    SDL.KeycodeC -> Just KeyboardButton_C
    SDL.KeycodeD -> Just KeyboardButton_D
    SDL.KeycodeE -> Just KeyboardButton_E
    SDL.KeycodeF -> Just KeyboardButton_F
    SDL.KeycodeG -> Just KeyboardButton_G
    SDL.KeycodeH -> Just KeyboardButton_H
    SDL.KeycodeI -> Just KeyboardButton_I
    SDL.KeycodeJ -> Just KeyboardButton_J
    SDL.KeycodeK -> Just KeyboardButton_K
    SDL.KeycodeL -> Just KeyboardButton_L
    SDL.KeycodeM -> Just KeyboardButton_M
    SDL.KeycodeN -> Just KeyboardButton_N
    SDL.KeycodeO -> Just KeyboardButton_O
    SDL.KeycodeP -> Just KeyboardButton_P
    SDL.KeycodeQ -> Just KeyboardButton_Q
    SDL.KeycodeR -> Just KeyboardButton_R
    SDL.KeycodeS -> Just KeyboardButton_S
    SDL.KeycodeT -> Just KeyboardButton_T
    SDL.KeycodeU -> Just KeyboardButton_U
    SDL.KeycodeV -> Just KeyboardButton_V
    SDL.KeycodeW -> Just KeyboardButton_W
    SDL.KeycodeX -> Just KeyboardButton_X
    SDL.KeycodeY -> Just KeyboardButton_Y
    SDL.KeycodeZ -> Just KeyboardButton_Z
    SDL.Keycode1 -> Just KeyboardButton_1
    SDL.Keycode2 -> Just KeyboardButton_2
    SDL.Keycode3 -> Just KeyboardButton_3
    SDL.Keycode4 -> Just KeyboardButton_4
    SDL.Keycode5 -> Just KeyboardButton_5
    SDL.Keycode6 -> Just KeyboardButton_6
    SDL.Keycode7 -> Just KeyboardButton_7
    SDL.Keycode8 -> Just KeyboardButton_8
    SDL.Keycode9 -> Just KeyboardButton_9
    SDL.Keycode0 -> Just KeyboardButton_0
    SDL.KeycodeKP1 -> Just KeyboardButton_NumPad1
    SDL.KeycodeKP2 -> Just KeyboardButton_NumPad2
    SDL.KeycodeKP3 -> Just KeyboardButton_NumPad3
    SDL.KeycodeKP4 -> Just KeyboardButton_NumPad4
    SDL.KeycodeKP5 -> Just KeyboardButton_NumPad5
    SDL.KeycodeKP6 -> Just KeyboardButton_NumPad6
    SDL.KeycodeKP7 -> Just KeyboardButton_NumPad7
    SDL.KeycodeKP8 -> Just KeyboardButton_NumPad8
    SDL.KeycodeKP9 -> Just KeyboardButton_NumPad9
    SDL.KeycodeKP0 -> Just KeyboardButton_NumPad0
    SDL.KeycodeF1 -> Just KeyboardButton_F1
    SDL.KeycodeF2 -> Just KeyboardButton_F2
    SDL.KeycodeF3 -> Just KeyboardButton_F3
    SDL.KeycodeF4 -> Just KeyboardButton_F4
    SDL.KeycodeF5 -> Just KeyboardButton_F5
    SDL.KeycodeF6 -> Just KeyboardButton_F6
    SDL.KeycodeF7 -> Just KeyboardButton_F7
    SDL.KeycodeF8 -> Just KeyboardButton_F8
    SDL.KeycodeF9 -> Just KeyboardButton_F9
    SDL.KeycodeF10 -> Just KeyboardButton_F10
    SDL.KeycodeF11 -> Just KeyboardButton_F11
    SDL.KeycodeF12 -> Just KeyboardButton_F12
    SDL.KeycodeBackquote -> Just KeyboardButton_Tilde
    SDL.KeycodeReturn -> Just KeyboardButton_Enter
    -- _ -> Just KeyboardButton_Ctrl
    -- _ -> Just KeyboardButton_Shift
    -- _ -> Just KeyboardButton_Alt
    SDL.KeycodeTab -> Just KeyboardButton_Tab
    SDL.KeycodeSpace -> Just KeyboardButton_Space
    SDL.KeycodeEscape -> Just KeyboardButton_Esc
    SDL.KeycodePrintScreen -> Just KeyboardButton_PrintScreen
    --  MouseButton_LeftClick
    --  MouseButton_RightClick
    --  MouseButton_MiddleClick
    --  MouseButton_WheelUp
    --  MouseButton_WheelDown

    _ -> Nothing
matchButton SDL.Released _ = Nothing

appRedraw :: AppState -> IO ()
appRedraw appState = do
  let renderer = view appRenderer appState
  SDL.rendererDrawColor renderer SDL.$= SDL.V4 0x00 0x00 0x00 0xff
  SDL.clear renderer
  -- TODO: Draw the game.
  SDL.present renderer
