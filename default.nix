let
  haskell-nix = import (builtins.fetchTarball "https://github.com/input-output-hk/haskell.nix/archive/749a7dc068d168b806d6f5d6e113c18a8b47475b.tar.gz") {};
  pkgs = import haskell-nix.sources.nixpkgs-2105 haskell-nix.nixpkgsArgs;
in
pkgs.haskell-nix.project {
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "d2clone";
    src = ./.;
  };
}
