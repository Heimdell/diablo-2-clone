module D2.SDL.Util
  ( setExceptionHandler,
    DrawAction,
    createTextureDrawAction
  ) where

import Data.Word (Word8)
import Foreign.C.Types (CInt)
import Data.String
import Control.Exception
import GHC.Conc (setUncaughtExceptionHandler)
import SDL qualified

import D2.Texture

setExceptionHandler :: Maybe SDL.Window -> IO ()
setExceptionHandler mwindow =
  setUncaughtExceptionHandler \exc -> do
    SDL.showSimpleMessageBox
      mwindow
      SDL.Error
      "Uncaught exception"
      (fromString (displayException exc))
    SDL.quit

type DrawAction = SDL.V2 CInt -> IO ()

createTextureDrawAction ::
  SDL.Renderer ->
  (Word8 -> SDL.V4 Word8) ->
  TextureGenerator ->
  IO DrawAction
createTextureDrawAction renderer palette TextureGenerator{tgWidth, tgHeight, tgFill} = do
  let w = fromIntegral tgWidth
      h = fromIntegral tgHeight
  surface <- SDL.createRGBSurface (SDL.V2 w h) SDL.RGBA8888
  surfaceRenderer <- SDL.createSoftwareRenderer surface
  tgFill \(x, y) color -> do
    SDL.rendererDrawColor surfaceRenderer SDL.$= palette color
    SDL.drawPoint surfaceRenderer (SDL.P (SDL.V2 (fromIntegral x) (fromIntegral y)))
  texture <- SDL.createTextureFromSurface renderer surface
  return \p -> do
    let dest = SDL.Rectangle (SDL.P p) (SDL.V2 w h)
    SDL.copy renderer texture Nothing (Just dest)
