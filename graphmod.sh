#!/usr/bin/env sh
graphmod --version
dot -V
hpack --version

tree src
hpack
graphmod -a -R Data -R Control -R GHC -R Foreign -R System | dot -Tsvg > "$1"
