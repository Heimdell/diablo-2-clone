module D2.Driver.DevTools (appDevTools) where

import Data.Foldable
import Data.Array as A
import Data.Word (Word8)
import Data.Traversable
import Control.Monad.State
import Control.Lens
import SDL qualified
import Data.Primitive.Array qualified as P.A

import qualified D2.Resources.Parser.Binary as R
import qualified D2.Resources.Parser.DT1 as R.DT1
import D2.Texture
import D2.SDL.Util

data AppState = AppState
  { _appWindow :: SDL.Window,
    _appRenderer :: SDL.Renderer,
    _appModeState :: AppModeState
  }

data AppModeState =
  BrowseTextures {
    _appModeIndex :: Int,
    _appModeTextures :: P.A.Array DrawAction
  }

makeLenses ''AppState
makeLenses ''AppModeState

appDevTools :: IO ()
appDevTools = do
  setExceptionHandler (Nothing @SDL.Window)
  SDL.initialize [SDL.InitVideo]
  window <-
    SDL.createWindow
      "Diablo II Clone - Dev Tools"
      SDL.defaultWindow
        { SDL.windowResizable = True }
  setExceptionHandler (Just window)
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer
  textures <- loadTextures renderer
  let appState =
        AppState {
          _appWindow = window,
          _appRenderer = renderer,
          _appModeState = BrowseTextures 0 textures
        }
  appRedraw appState   -- Draw the window for the first time,
                       -- before we handle any events.
  appLoop appState
  SDL.quit

pprInfo :: AppModeState -> String
pprInfo (BrowseTextures i ts) =
  "Texture #" ++ show i ++ " out of " ++ show (length ts)

navKey :: SDL.Keycode -> Maybe Int
navKey SDL.KeycodeJ = Just 1
navKey SDL.KeycodeK = Just (-1)
navKey _ = Nothing

appLoop :: AppState -> IO ()
appLoop appState = do
  event <- SDL.waitEvent
  case SDL.eventPayload event of
    SDL.QuitEvent -> return ()
    SDL.WindowSizeChangedEvent{} -> do
      appRedraw appState
      appLoop appState
    SDL.KeyboardEvent kd
      | SDL.Pressed <- SDL.keyboardEventKeyMotion kd,
        Just offset <- navKey (SDL.keysymKeycode (SDL.keyboardEventKeysym kd))
      -> do
        let appState' = flip execState appState do
              zoom appModeState do
                n <- uses appModeTextures length
                appModeIndex += offset
                appModeIndex %= max 0 . min (n-1)
        putStrLn (pprInfo (view appModeState appState'))
        appRedraw appState'
        appLoop appState'
    _ -> appLoop appState

appRedraw :: AppState -> IO ()
appRedraw appState = do
  let renderer = view appRenderer appState
      modeState = view appModeState appState
  SDL.rendererDrawColor renderer SDL.$= SDL.V4 0x00 0x00 0x00 0xff
  SDL.clear renderer
  case modeState of
    BrowseTextures i ts -> do
      let t = P.A.indexArray ts i
      t (SDL.V2 32 32)
  SDL.present renderer

loadTextures :: SDL.Renderer -> IO (P.A.Array DrawAction)
loadTextures renderer = do
  Right parsedHeader <- R.parseFile "data/global/tiles/ACT1/TOWN/objects.dt1" R.DT1.header
  Right act1palette <- R.parseFile "data/global/palette/ACT1/pal.dat" R.DT1.palette
  fmap P.A.fromList do
    let p = rD2PaletteToPalette act1palette
    for (toList (R.DT1.tileHeaders parsedHeader)) \t -> do
      let tg = textureGeneratorFromD1 (R.DT1.texture t)
      createTextureDrawAction renderer p tg

rD2PaletteToPalette :: R.DT1.Palette -> Word8 -> SDL.V4 Word8
rD2PaletteToPalette (R.DT1.Palette k) = (arr !)
  where arr = k (\r g b -> SDL.V4 r g b 0xff)

textureGeneratorFromD1 :: R.DT1.Filler -> TextureGenerator
textureGeneratorFromD1 dt1 =
  TextureGenerator {
    tgWidth   = R.DT1.fWidth dt1,
    tgHeight  = R.DT1.fHeight dt1,
    tgFill    = R.DT1.fFill dt1 id
  }
