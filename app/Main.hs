import D2.Driver.Game (appGame)
import D2.Driver.DevTools (appDevTools)

import System.Environment (getArgs)

main :: IO ()
main = do
  args <- getArgs
  case args of
    "tools" : _ -> appDevTools
    _ -> appGame
